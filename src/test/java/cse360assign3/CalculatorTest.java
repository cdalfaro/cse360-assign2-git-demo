/*
 * Student: Christian Alfaro (cdalfaro)
 * ClassID: 85141 - CSE360 - Software Engineering
 * Assign#: 3
 * Content: Calculator Testing Suite (Junit5 Jupiter)
 *
 * Repository: https://bitbucket.org/cdalfaro/cse360-assign2-git-demo/src/master/
 */

package cse360assign3;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Calculator Testing Suite
 */
class CalculatorTest {
    private Calculator calc;

    @BeforeEach
    void beforeEach() {
        calc = new Calculator();
    }

    @Test
    @DisplayName("mult(): basic multiplication")
    void multCase00() {
        final int EXPECTED = 15;
        calc.add(5);
        calc.mult(3);
        final int ACTUAL = calc.getTotal();
        assertEquals(EXPECTED, ACTUAL, "should multiply the current value and save it into total");
    }

    @Test
    @DisplayName("mult(): multiplication by zero")
    void multCase01() {
        final int EXPECTED = 0;
        calc.add(5);
        calc.mult(0);
        final int ACTUAL = calc.getTotal();
        assertEquals(EXPECTED, ACTUAL, "multiplication by zero should result in a value of 0");
    }

    @Test
    @DisplayName("mult(): multiplication by negatives")
    void multCase02() {
        final int EXPECTED = -15;
        calc.add(5);
        calc.mult(-3);
        final int ACTUAL = calc.getTotal();
        assertEquals(EXPECTED, ACTUAL, "multiplication by negatives should result in the negated value");
    }

    @Test
    @DisplayName("mult(): basic tostring()")
    void multCase03() {
        final String EXPECTED = "0 * 5";
        calc.mult(5);
        final String ACTUAL = calc.toString();
        assertEquals(EXPECTED, ACTUAL, "mult() should record history of ops");
    }

    @Test
    @DisplayName("div(): basic division")
    void divideCase00() {
        final int EXPECTED = 2;
        calc.add(10);
        calc.div(5);
        final int ACTUAL = calc.getTotal();
        assertEquals(EXPECTED, ACTUAL, "basic division result should be saved to total");
    }

    @Test
    @DisplayName("div(): proper integer division")
    void divideCase01() {
        final int EXPECTED = 2;
        calc.add(14);
        calc.div(5);
        final int ACTUAL = calc.getTotal();
        assertEquals(EXPECTED, ACTUAL, "div() should result in proper floor(integerDivision) value");
    }

    @Test
    @DisplayName("div(): value after divide by 0")
    void divideCase02() {
        final int EXPECTED = 0;
        calc.add(5);
        calc.div(0);
        final int ACTUAL = calc.getTotal();
        assertEquals(EXPECTED, ACTUAL, "div(0) should result in new total of 0");
    }

    @Test
    @DisplayName("div(): history after divide by 0")
    void divideCase03() {
        final String EXPECTED = "0 + 5 / 0";
        calc.add(5);
        calc.div(0);
        final String ACTUAL = calc.toString();
        assertEquals(EXPECTED, ACTUAL, "div(0) should not reset history");
    }

    @Test
    @DisplayName("div(): basic tostring()")
    void divideCase04() {
        final String EXPECTED = "0 / 5";
        calc.div(5);
        final String ACTUAL = calc.toString();
        assertEquals(EXPECTED, ACTUAL, "div() should record history of ops");
    }

    @Test
    @DisplayName("power(): basic power test")
    void powerCase00() {
        final int EXPECTED = 256;
        calc.add(2);
        calc.power(8);
        final int ACTUAL = calc.getTotal();
        assertEquals(EXPECTED, ACTUAL, "pow() should result in value of 'total raised to power'");
    }

    @Test
    @DisplayName("power(): zeroth power")
    void powerCase01() {
        final int EXPECTED = 1;
        calc.add(2);
        calc.power(0);
        final int ACTUAL = calc.getTotal();
        assertEquals(EXPECTED, ACTUAL, "pow(0) should result in value of 1");
    }

    @Test
    @DisplayName("power(): 1th power")
    void powerCase02() {
        final int EXPECTED = 7;
        calc.add(7);
        calc.power(1);
        final int ACTUAL = calc.getTotal();
        assertEquals(EXPECTED, ACTUAL, "pow(1) should result in unchanged value");
    }

    @Test
    @DisplayName("power(): value after negative power")
    void powerCase03() {
        final int EXPECTED = 0;
        calc.add(7);
        calc.power(-5);
        final int ACTUAL = calc.getTotal();
        assertEquals(EXPECTED, ACTUAL, "pow(negative) should result in value of zero");
    }

    @Test
    @DisplayName("power(): history after negative power")
    void powerCase04() {
        final String EXPECTED = "0 + 7 ^ -5";
        calc.add(7);
        calc.power(-5);
        final String ACTUAL = calc.toString();
        assertEquals(EXPECTED, ACTUAL, "pow(negative) should not reset history");
    }

    @Test
    @DisplayName("power(): basic tostring()")
    void powerCase05() {
        final String EXPECTED = "0 ^ 5";
        calc.power(5);
        final String ACTUAL = calc.toString();
        assertEquals(EXPECTED, ACTUAL, "power() should record history of ops");
    }
}