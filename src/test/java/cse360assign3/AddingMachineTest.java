/*
 * Student: Christian Alfaro (cdalfaro)
 * ClassID: 85141 - CSE360 - Software Engineering
 * Assign#: 3
 * Content: Adding machine test suite JUnit5 Jupiter
 *
 * Repository: https://bitbucket.org/cdalfaro/cse360-assign2-git-demo/src/master/
 */

package cse360assign3;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test suite for the AddingMachine class
 */
class AddingMachineTest {
    private AddingMachine machine;

    @BeforeEach
    void beforeEach() {
        machine = new AddingMachine();
    }

    @Test
    @DisplayName("getTotal(): basic functionality")
    void getTotal() {
        final int EXPECTED = 0;
        int actual = machine.getTotal();
        assertEquals(EXPECTED, actual, "should return current amount in the machine");
    }

    @Test
    @DisplayName("Add(): basic functionality")
    void addCase00() {
        final int EXPECTED = 5;
        machine.add(5);
        int actual = machine.getTotal();
        assertEquals(EXPECTED, actual, "should return the value of 0 + 5 = 5");
    }

    @Test
    @DisplayName("Add(): multiple additions")
    void addCase01() {
        final int EXPECTED = 9;
        machine.add(5);
        machine.add(4);
        int actual = machine.getTotal();
        assertEquals(EXPECTED, actual, "should return the value of 0 + 5 + 4 = 9");
    }

    @Test
    @DisplayName("Add(): should continue to work after clear()")
    void addCase02() {
        final int EXPECTED = 3;
        machine.add(5);
        machine.clear();
        machine.add(3);
        int actual = machine.getTotal();
        assertEquals(EXPECTED, actual, "machine should continue to track ops after clear()");
    }

    @Test
    @DisplayName("Subtract(): basic functionality")
    void subtractCase00() {
        final int EXPECTED = -5;
        machine.subtract(5);
        int actual = machine.getTotal();
        assertEquals(EXPECTED, actual, "should return the value of 0 - 5 = -5");
    }

    @Test
    @DisplayName("Subtract(): multiple subtractions")
    void subtractCase01() {
        final int EXPECTED = -9;
        machine.subtract(5);
        machine.subtract(4);
        int actual = machine.getTotal();
        assertEquals(EXPECTED, actual, "should return the value of 0 - 5 - 4 = -9");
    }

    @Test
    @DisplayName("Subtract(): should continue to work after clear()")
    void subtractCase02() {
        final int EXPECTED = 3;
        machine.subtract(5);
        machine.clear();
        machine.subtract(-3);
        int actual = machine.getTotal();
        assertEquals(EXPECTED, actual, "machine should continue to track ops after clear()");
    }

    @Test
    @DisplayName("toString(): initial machine state")
    void testToStringCase00() {
        final String EXPECTED = "0";
        String actual = machine.toString();
        assertEquals(EXPECTED, actual, "Should return the initial value of 0 as a string");
    }

    @Test
    @DisplayName("toString(): addition & subtraction usage")
    void testToStringCase01() {
        final String EXPECTED = "0 + 4 - 2 + 5";
        machine.add(4);
        machine.subtract(2);
        machine.add(5);
        String actual = machine.toString();
        assertEquals(EXPECTED, actual, "Should return operation & value each separated by space characters");
    }

    @Test
    @DisplayName("toString(): account for negative number input")
    void testToStringCase02() {
        final String EXPECTED = "0 + -5 - -5";
        machine.add(-5);
        machine.subtract(-5);
        String actual = machine.toString();
        assertEquals(EXPECTED, actual, "Negative numbers should be preceded by a negative sign");
    }

    @Test
    @DisplayName("toString(): continues to track history after clear()")
    void testToSTringCase03() {
        final String EXPECTED = "0 + 5";
        machine.add(2);
        machine.clear();
        machine.add(5);
        String actual = machine.toString();
        assertEquals(EXPECTED, actual, "machine should continue to track history after clear()");
    }

    @Test
    @DisplayName("clear(): resets contents to zero")
    void clearTest00() {
        final int EXPECTED_TOTAL = 0;

        machine.add(5);
        machine.subtract(4);
        machine.add(-3);
        machine.subtract(-1);
        machine.clear();

        int actualTotal = machine.getTotal();
        assertEquals(EXPECTED_TOTAL, actualTotal, "clear() should reset counter to value zero");
    }

    @Test
    @DisplayName("clear(): clears history")
    void clearTest01() {
        final String EXPECTED_TOSTRING = "0";

        machine.add(5);
        machine.subtract(4);
        machine.add(-3);
        machine.subtract(-1);
        machine.clear();

        String actualToString = machine.toString();
        assertEquals(EXPECTED_TOSTRING, actualToString, "clear() should reset history to the empty state");
    }
}