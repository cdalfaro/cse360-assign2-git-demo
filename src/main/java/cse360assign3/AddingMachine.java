/*
 * Student: Christian Alfaro (cdalfaro)
 * ClassID: 85141 - CSE360 - Software Engineering
 * Assign#: 3
 * Content: Adding machine class
 *
 * Repository: https://bitbucket.org/cdalfaro/cse360-assign2-git-demo/src/master/
 */

package cse360assign3;

/**
 * Adder + subtraction functionality, records history of operations and may be retrieved with toString()
 */
public class AddingMachine {
    protected int total;
    protected String history;

    /**
     * Constructor initializes with total of 0
     */
    public AddingMachine() {
        clear();
    }

    /**
     * Retrieve the current total
     * @return current total value after all arithmetic operations
     */
    public int getTotal() {
        return total;
    }

    /**
     * Adds value to the machine via basic integer addition
     * @param value Addend
     */
    public void add(int value) {
        total += value;
        history += (" + " + value);
    }

    /**
     * Subtracts value from the machine via basic integer subtraction
     * @param value Subtrahend
     */
    public void subtract(int value) {
        total -= value;
        history += (" - " + value);
    }

    /**
     * Returns a log of add/subtract operations performed in order from start to finish
     * @return history of add/subtract operations<br>
     *     If the following operations took place:
     *     <ol>
     *         <li>add(4);</li>
     *         <li>subtract(2);</li>
     *         <li>add(5);</li>
     *     </ol>
     *     The resulting toString would return "0 + 4 - 2 + 5", where 0 is the initial value
     */
    public String toString() {
        return history;
    }

    /**
     * Sets total to 0, empties history of operations
     */
    public void clear() {
        total = 0;
        history = "0";
    }
}
