/*
 * Student: Christian Alfaro (cdalfaro)
 * ClassID: 85141 - CSE360 - Software Engineering
 * Assign#: 3
 * Content: Calculator Class
 *
 * Repository: https://bitbucket.org/cdalfaro/cse360-assign2-git-demo/src/master/
 */

package cse360assign3;

/**
 * Calculator implementing multiplication, division, and power operators onto AddingMachine functionality
 */
public class Calculator extends AddingMachine {

    /**
     * Trivial default constructor initializes total to 0, history to "0"
     */
    public Calculator() {
        super();
    }

    /**
     * Multiplies value in the machine by multiplier
     * @param multiplier amount to multiply current value in memory by
     */
    public void mult(int multiplier) {
        total *= multiplier;
        history += (" * " + multiplier);
    }

    /**
     * Divides value in the machine by the divisor; if 0, sets total to 0.
     * @param divisor amount to divide current value in memory by
     */
    public void div(int divisor) {
        if (divisor == 0) {
            total = 0;
        }
        else {
            total /= divisor;
        }

        history += (" / " + divisor);
    }

    /**
     * Raises value in machine to the power of the value in 'power'; if negative, sets total to 0
     * @param power amount to raise the current value in memory by
     */
    public void power(int power) {
        if (power < 0) {
            total = 0;
        }
        else if (power == 0) {
            total = 1;
        }
        else if (power >= 2) {
            total = power(total, power);
        }

        history += (" ^ " + power);
    }

    /**
     * Interface for deriving a power; assumes power is non-negative, non-zero
     * @param base exponent base
     * @param power exponent power
     * @return the value of base^power if assumptions are met
     */
    private int power(int base, int power) {
        return powerHelper(base, base, power);
    }

    // recursion helper; only to be invoked by power/2
    private int powerHelper(int currentBase, int originalBase, int remainingPower) {
        if (remainingPower == 1) {
            return currentBase;
        }

        return powerHelper(currentBase * originalBase, originalBase, remainingPower - 1);
    }
}
